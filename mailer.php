<?php
require 'vendor/autoload.php';

use LSMailer\LSMail;

try {
    # for a gmail account use:
    #   $conf->host = 'smtp.gmail.com';
    #   $conf->port = 465;
    #   $conf->secure = 'ssl';
    #
    # for live.com (outlook.com)
    #   $conf->host = 'smtp.live.com';
    #   $conf->port = 587;
    #   $conf->secure = 'tls';

    $conf = new stdClass;
    $conf->host = 'mail.example.com';
    $conf->port = 465;
    $conf->secure = 'ssl';
    $conf->user = '_some_user_';
    $conf->pass = '_some_pass_';


    $mailer = LSMail::factory('smtp', $conf);

    # multiple recipients could be inserted as an array
    $mailer->to(array('first_email@lemonsoftware.eu', 'second_email@lemonsoftware.eu'));

    # or just as single email addresses
    $mailer->to('third_email@lemonsoftware.eu');
    $mailer->from('sender@example.com');

    # full format email + name or just email
    $mailer->cc('lsmailer@lemonsoftware.eu; LSMailer');
    $mailer->reply_to('lsmailer@lemonsoftware.eu'); # no name here

    $mailer->subject("You got mail!");

    # text or html
    $mailer->format('html');
    # or $mailer->format('text');

    # 1 to print out debugging messages; usually 0 in a production env
    $mailer->debug(1);

    # single file as attachment
    $mailer->attach('/path/to/file1');
    # or multiple files
    $mailer->attach(array('/path/to/file2', '/path/to/file3'));

    # html body; if you set format as text, but use html tags, these will be removed from output
    $mailer->body("This is <i>simple</i> html example.<br> Bang!");

    $resp = $mailer->send();

    # true for success
    var_dump($resp);
} catch (\Exception $e) {
    echo $e->getMessage();
}

