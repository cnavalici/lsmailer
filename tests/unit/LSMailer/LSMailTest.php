<?php namespace LSMailer;

use LSMailer\LSMail;

class LSMailTest extends \PHPUnit_Framework_TestCase {
    public function testInitializedNoArguments() {
        $this->setExpectedException('Exception');
        LSMail::factory();
    }


    public function testInitializedWrongDriver() {
        $this->setExpectedException('Exception');
        LSMail::factory('wrong_driver', new \stdClass);
    }

    public function testInitializedSMTPDriver() {
        $factored = LSMail::factory('smtp', new \stdClass);
        $this->assertInstanceOf('LSMailer\Drivers\LSSmtp', $factored);
    }
}


