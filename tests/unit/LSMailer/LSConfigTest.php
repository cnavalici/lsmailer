<?php namespace LSMailer;

use LSMailer\Config\LSConfig;

class LSConfigTest extends \PHPUnit_Framework_TestCase {
    public function testInitializedNoArguments() {
        $this->setExpectedException('Exception');
        new LSConfig();
    }

    public function testConfigurationCommonElements() {
        $conf = new \stdClass;
        $elem = array('host' => 'example.com', 'port' => 123, 'secure' => '', 'user' => 'john_doe', 'pass' => 'password123');
        foreach ($elem as $ek => $ev) {
            $conf->{$ek} = $ev;
        }

        $obj = new LSConfig($conf);
        foreach ($elem as $ek => $ev) {
            $prop = $this->reflect($obj, $ek);
            $this->assertEquals($prop->getValue($obj), $ev);
        }
    }


    public function testConfigurationHostSecure() {
        $conf = new \stdClass;
        $elem = array('host' => 'example.com', 'secure' => 'ssl');
        foreach ($elem as $ek => $ev) {
            $conf->{$ek} = $ev;
        }

        $obj = new LSConfig($conf);
        $prop = $this->reflect($obj, 'host');

        $this->assertEquals($prop->getValue($obj), 'ssl://example.com');
    }

    public function testConfigurationInvalidData() {
        $test_data = new \stdClass;
        $test_data->host = 'localhost';
        $test_data->wrong_property = 'tls';

        $obj = new LSConfig($test_data);

		// extract the properties
        foreach($this->cls->getProperties() as $p) $props[] = $p->name;

        $this->assertTrue(in_array('host', $props));
        $this->assertFalse(in_array('wrong_property', $props));
    }

    public function testSetupTimezone() {
        $test_data = new \stdClass;
        $test_data->timezone = 'Europe/Bucharest';

        $obj = new LSConfig($test_data);

        $current = date_default_timezone_get();
        $this->assertEquals($current, $test_data->timezone);
    }

    public function setUp() {
        $this->cls = new \ReflectionClass('LSMailer\Config\LSConfig');
    }

    private function reflect($obj, $name) {
        $prop = new \ReflectionProperty($obj, $name);
        $prop->setAccessible(true);
        return $prop;
    }


    private function reflect_method($name) {
        $method = $this->cls->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }
}
