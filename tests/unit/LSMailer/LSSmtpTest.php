<?php namespace LSMailer;
/**
 *
 * phpunit --bootstrap=tests/bootstrap.php tests/unit/LSMailer/LSSmtpTest
 * code coverage is given by phpunit.xml.dist
 */

use LSMailer\Drivers\LSSmtp;

class LSSmtpTest extends \PHPUnit_Framework_TestCase {

    public function testParseEmailsNoData() {
        $method = $this->reflect_method('parse_emails');

        $obj = new LSSmtp(new \stdClass);
        $this->assertTrue(is_object($method->invoke($obj, 'to')));
    }


    public function testPopulateArrayOfData() {
        $test_data = array('test@example;John Doe', 'rudy@example.com;Rudy Beacon', 'noname@example.com');
        $expected_data = array(array('email' => 'test@example', 'name' => 'John Doe'),
                            array('email' => 'rudy@example.com', 'name' => 'Rudy Beacon'),
                            array('email' => 'noname@example.com', 'name' => ''));

        $obj = new LSSmtp(new \stdClass);

        $fields = array('to', 'cc', 'bcc');
        foreach ($fields as $f) {
            $obj->{$f}($test_data);
            $prop = $this->reflect($obj, $f);
            $this->assertEquals($prop->getValue($obj), $expected_data);
        }
    }


    public function testPopulateSimpleData() {
        $test_data = array('test@example;John Doe');
        $expected_data = array(array('email' => 'test@example', 'name' => 'John Doe'));

        $obj = new LSSmtp(new \stdClass);

        $fields = array('to', 'cc', 'bcc', 'reply_to');
        foreach ($fields as $f) {
            $obj->{$f}($test_data);
            $prop = $this->reflect($obj, $f);

            $this->assertEquals($prop->getValue($obj), $expected_data);
        }
    }


    public function testFromFieldGoodData() {
        $test_data_set = array(
            'test@example;John Doe',
            array('test@example;John Doe')
        );
        $expected_data = array(array('email' => 'test@example', 'name' => 'John Doe'));

        foreach($test_data_set as $test_data) {
            $obj = new LSSmtp(new \stdClass);
            $obj->from($test_data);
            $prop = $this->reflect($obj, 'from');

            $this->assertEquals($prop->getValue($obj), $expected_data);
        }
    }

    public function testFormatSetterGoodData() {
        $test_data = array('text', 'html');

        $obj = new LSSmtp(new \stdClass);
        foreach ($test_data as $td) {
            $obj->format($td);

            $prop = $this->reflect($obj, 'format');
            $this->assertEquals($prop->getValue($obj), $td);
        }
    }


    public function testFormatSetterInvalidData() {
        $test_data = array('texta', '', 'html+text');

        $obj = new LSSmtp(new \stdClass);
        $prop = $this->reflect($obj, 'format');
        $default_value = $prop->getValue($obj);

        foreach ($test_data as $td) {
            $method = $this->reflect_method('parse_emails');
            $obj->format($td);

            $prop = $this->reflect($obj, 'format');
            $this->assertEquals($default_value, $prop->getValue($obj));
        }
    }


    public function testPrepareRecipientGoodData() {
        $test_data = array(
            0 => array('email' => 'test@example', 'name' => 'John Doe'),
            1 => array('email' => 'test@example', 'name' => ''),
            2 => array('email' => '', 'name' => ''));

        $expected_data = array(
            0 => 'John Doe<test@example>',
            1 => '<test@example>',
            2 => '<>');

        $method = $this->reflect_method('prepare_recipient');

        foreach ($test_data as $k => $td) {
            $obj = new LSSmtp(new \stdClass);

            $resp = $method->invoke($obj, $td);
            $this->assertEquals($resp, $expected_data[$k]);
        }
    }

    public function testSubjectFieldValidData() {
        $test_data = 'some kind of a subject';
        $expected_data = 'some kind of a subject';

        $obj = new LSSmtp(new \stdClass);
        $obj->subject($test_data);

        $prop = $this->reflect($obj, 'subject');

        $this->assertEquals($prop->getValue($obj), $expected_data);
    }


    public function testSubjectFieldVeryLong() {
        $test_data = str_repeat('AAAA-AAAA-AA', 2000);

        $obj = new LSSmtp(new \stdClass);
        $obj->subject($test_data);

        $prop = $this->reflect($obj, 'subject');

        $this->assertEquals(strlen($prop->getValue($obj)), 998);
    }


    public function testBodySetterGoodData() {
        $test_data = 'ABC<b>this is a test</b>';

        $obj = new LSSmtp(new \stdClass);
        $obj->body($test_data);

        $prop = $this->reflect($obj, 'body');

        $this->assertEquals($prop->getValue($obj), $test_data);
    }


    public function testDebugFlag() {
        $obj = new LSSmtp(new \stdClass);
        $test_data_true = array('1', 1, True, 'asdasd');
        $test_data_false = array('0', 0, False, '');

        $obj = new LSSmtp(new \stdClass);

        foreach ($test_data_true as $td) {
            $obj->debug($td);

            $prop = $this->reflect($obj, 'debug');
            $this->assertTrue($prop->getValue($obj));
        }

        foreach ($test_data_false as $td) {
            $obj->debug($td);

            $prop = $this->reflect($obj, 'debug');
            $this->assertFalse($prop->getValue($obj));
        }
    }


    public function testBuildHeaders() {
        $obj = new LSSmtp(new \stdClass);
        $obj->from('john.doe@example.com; John Doe');
        $obj->to('jane.doe@example.com; Jane Doe');
        $obj->subject("The subject is here");

        $method = $this->reflect_method('build_headers');

        $current_date = date('r');
        $returned_data = $method->invoke($obj);

        $expected_data = "From: John Doe<john.doe@example.com>\r\n".
                "Reply-To: John Doe<john.doe@example.com>\r\n".
                "Subject: The subject is here\r\n".
                "Date: $current_date\r\n".
                "User-Agent: LSMailer\r\n".
                "To: Jane Doe<jane.doe@example.com>\r\n";
        $this->assertEquals($expected_data, $returned_data);
    }


    public function testOut() {
        $test_data = '204 ABCDE';
        $obj = new LSSmtp(new \stdClass);

        $tf = tmpfile();

        $prop = $this->reflect($obj, 'connection');
        $prop->setValue($obj, $tf);

        $method = $this->reflect_method('out');

        $method->invoke($obj, $test_data);
        fseek($tf, 0);

        $this->assertEquals($test_data, rtrim(fread($tf, 4096)));
    }


    public function testIn() {
        $test_data = '204 ABCDE';
        $expected_data = array('204 ABCDE', '204');
        $obj = new LSSmtp(new \stdClass);

        $tf = tmpfile();
        fwrite($tf, $test_data);
        rewind($tf);

        $prop = $this->reflect($obj, 'connection');
        $prop->setValue($obj, $tf);

        $method = $this->reflect_method('in');
        $resp = $method->invoke($obj);

        $this->assertEquals($resp, $expected_data);
    }


    public function testInOutNoConnection() {
        $obj = new LSSmtp(new \stdClass);

        $prop = $this->reflect($obj, 'connection');
        $prop->setValue($obj, NULL);

        foreach(array('in', 'out') as $method) {
            $method = $this->reflect_method('out');

            $resp = $method->invoke($obj, 'doesnt matter');
            $this->assertFalse($resp);
        }
    }


    public function testPrepareTextSection() {
        $test_data = 'This is a test';
        $expected_data = 'Content-Type: text/plain; charset='.$this->config->charset.' '.$this->nl.$this->nl.
                        //'Content-Transfer-Encoding: '.$this->encoding.' '.$this->nl.$this->nl.
                        $test_data.' '.$this->nl;

        $obj = new LSSmtp(new \stdClass);
        $obj->body($test_data);

        $method = $this->reflect_method('prepare_text_section');
        $resp = $method->invoke($obj, '');

        $this->assertEquals($resp, $expected_data);
    }


    public function testPrepareHTMLSection() {
        $test_data = '<b>This is a test</b>';
        $expected_data = 'Content-Type: text/html; charset='.$this->config->charset.' '.$this->nl. $this->nl.
                        //'Content-Transfer-Encoding: '.$this->encoding.' '.$this->nl.$this->nl.
                        $test_data.' '.$this->nl;

        $obj = new LSSmtp(new \stdClass);
        $obj->body($test_data);

        $method = $this->reflect_method('prepare_html_section');
        $resp = $method->invoke($obj, '');

        $this->assertEquals($resp, $expected_data);
    }


    public function testPrepareMultipartSection() {
        $expected_data = 'MIME-Version: 1.0%a'.
                        'Content-Type: multipart/alternative; boundary=%a'.
                        'This is a multi-part message in MIME format.%a';

        $obj = new LSSmtp(new \stdClass);

        $method = $this->reflect_method('prepare_multipart_section');
        $resp = $method->invoke($obj, '');

        $this->assertStringMatchesFormat($expected_data, $resp);
    }


    public function testAttachSingle() {
        $test_data = '/tmp/single.zip';

        $obj = new LSSmtp(new \stdClass);
        $obj->attach($test_data);
        $prop = $this->reflect($obj, 'attachments');

        $this->assertEquals($prop->getValue($obj), array($test_data));
    }


    public function testAttachMultiple() {
        $test_data = array('/tmp/single.zip', '/tmp/double.zip');

        $obj = new LSSmtp(new \stdClass);
        $obj->attach($test_data);
        $prop = $this->reflect($obj, 'attachments');

        $this->assertEquals($prop->getValue($obj), $test_data);
    }


    public function testParseEmail() {
        $test_data = array( 0 => 'some@test.com', 1 => 'some@test.com; John Doe');
        $expected_data = array(
            0 => array('email' => 'some@test.com', 'name' => ''),
            1 => array('email' => 'some@test.com', 'name' => 'John Doe')
        );

        $obj = new LSSmtp(new \stdClass);

        $method = $this->reflect_method('parse_single_email');
        foreach ($test_data as $k => $v) {
            $resp = $method->invoke($obj, $v);
            $this->assertEquals($resp, $expected_data[$k]);
        }
    }


    public function testParseEmails() {
        $test_data = array(
            0 => 'some@test.com',
            1 => 'some@test.com; John Doe',
            2 => array('some@test.com', 'extra@example.com'),
            3 => array('some@test.com', 'extra@example.com; Extra Time ')
        );
        $expected_data = array(
            0 => array(array('email' => 'some@test.com', 'name' => '')),
            1 => array(array('email' => 'some@test.com', 'name' => 'John Doe')),
            2 => array( array('email' => 'some@test.com', 'name' => ''),
                        array('email' => 'extra@example.com', 'name' => '') ),
            3 => array( array('email' => 'some@test.com', 'name' => ''),
                        array('email' => 'extra@example.com', 'name' => 'Extra Time') )
        );
        $sections = array('to', 'bcc', 'cc');

        $obj = new LSSmtp(new \stdClass);

        $method = $this->reflect_method('parse_emails');
        foreach ($sections as $sec) {
            foreach ($test_data as $k => $v) {
                $method->invoke($obj, $sec, $v);

                $prop = $this->reflect($obj, $sec);
                $current_value = $prop->getValue($obj);
                $prop->setValue($obj, []); // reset

                $this->assertEquals($current_value, $expected_data[$k]);
            }
        }
    }


    public function setUp() {
        $this->cls = new \ReflectionClass('LSMailer\Drivers\LSSmtp');

        $obj = new LSSmtp(new \stdClass);
        foreach(array('sep', 'encoding', 'nl', 'config') as $p) {
            $prop = $this->reflect($obj, $p);
            $this->{$p} = $prop->getValue($obj);
        }
    }


    private function reflect($obj, $name) {
        $prop = new \ReflectionProperty($obj, $name);
        $prop->setAccessible(true);
        return $prop;
    }


    private function reflect_method($name) {
        $method = $this->cls->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }
}
