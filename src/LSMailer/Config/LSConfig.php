<?php namespace LSMailer\Config;
/**
 * LSConfig
 *
 * @author Cristian NĂVĂLICI <ncristian@lemonsoftware.eu>
 * @license @see LICENSE.txt
 * @package LSMailer
 */

class LSConfig {
    public $host = '';
    public $port = '25';
    public $secure = '';
    public $user = '';
    public $pass = '';
    public $charset = 'UTF-8';

    private $timezone = 'Europe/Amsterdam';

    private static $allowed_properties = array('host', 'port', 'user', 'pass', 'secure', 'charset', 'timezone');

    public function __construct($data) {
        $this->parse_configuration_data($data);
        $this->setup_timezone();
    }

    private function parse_configuration_data($data) {
        // here it might be implemented different parsers for input
        // $data might be stdClass, array, etc
        if ($data instanceof \stdClass) {
            foreach(get_object_vars($data) as $conf => $value) {
                if (in_array($conf, self::$allowed_properties) && !empty($value)) {
                    $this->{$conf} = $value;
                }
            }
        }

        // special case
        if ($this->secure === 'ssl') {
            $this->host = 'ssl://'.$this->host;
        }
    }

    private function setup_timezone() {
        date_default_timezone_set($this->timezone);
    }
}