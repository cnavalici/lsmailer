<?php namespace LSMailer\Config;
/**
 * LSMessages
 *
 * @author Cristian NĂVĂLICI <ncristian@lemonsoftware.eu>
 * @license @see LICENSE.txt
 * @package LSMailer
 */

class LSMessages {
    const MSG_SENDNOW = 'Sending now...';
    const MSG_EHLONOTACCEPTED = 'EHLO command not accepted from the server';
    const MSG_AUTHLOGINFAILED = 'AUTH LOGIN failed';
    const MSG_AUTHLOGINFAILEDUSER = 'AUTH LOGIN failed for user';
    const MSG_AUTHLOGINFAILEDPASS = 'AUTH LOGIN failed for password';
    const MSG_UNABLESTARTTLS = 'Unable to start TLS';
    const MSG_DATANOTACCEPTED = 'Command DATA not accepted by the server';
}