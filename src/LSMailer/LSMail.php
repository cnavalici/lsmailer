<?php namespace LSMailer;
/**
 * LSMail
 *
 * @author Cristian NĂVĂLICI <ncristian@lemonsoftware.eu>
 * @license @see LICENSE.txt
 * @package LSMailer
 */

use LSMailer\Drivers\LSSmtp;

class LSMail {
    public static function factory($driver_name, \stdClass $data) {
        switch ($driver_name) {
            case 'smtp':
                return new LSSmtp($data);
            break;
            default:
                throw new \Exception("You need to specify a name for the driver");
        }
    }
}
