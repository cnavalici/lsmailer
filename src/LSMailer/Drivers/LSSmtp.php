<?php namespace LSMailer\Drivers;
/**
 * LSSmtp
 *
 * @author Cristian NĂVĂLICI <ncristian@lemonsoftware.eu>
 * @license @see LICENSE.txt
 * @package LSMailer
 */

use LSMailer\Config\LSConfig;
use LSMailer\Config\LSMessages as _;

class LSSmtp {
    protected $debug = False;

    /**
     * @var LSMailer\Config\LSConfig
     */
    private $config;

    // internal variables
    private $connection = null;
    private $content = '';
    private $headers = '';
    private $boundary = '';

    // variables bound to setters
    protected $to = [];
    protected $cc = [];
    protected $bcc = [];
    protected $from = [];
    protected $reply_to = [];
    protected $subject = '';
    protected $format = 'text';
    protected $body = '';
    protected $attachments = [];

    // internal static variables
    protected static $nl = "\r\n";
    protected static $sep = ";";
    protected static $encoding = '7bit';
    protected static $timeout = 30;
    protected static $wordwrap = 70;
    protected static $sending_host = 'localhost';

    public function __construct($data) {
        $this->config = new LSConfig($data);
    }

    public function send() {
        $this->printer(_::MSG_SENDNOW);
        $this->headers = $this->build_headers();
        $this->build_content();

        if ($this->connect()) {
            $delivered = $this->deliver();
            $this->disconnect();

            return $delivered;
        } else {
            return false;
        }
    }


    public function to($data = '') {
        return $this->parse_emails('to', $data);
    }


    public function cc($data = '') {
        return $this->parse_emails('cc', $data);
    }


    public function bcc($data = '') {
        return $this->parse_emails('bcc', $data);
    }


    public function from($data = '') {
        return $this->parse_emails('from', $data);
    }


    public function reply_to($data = '') {
        return $this->parse_emails('reply_to', $data);
    }


    public function subject($data = '') {
        $subject = trim($data);
        if ($subject) {
            # http://www.faqs.org/rfcs/rfc2822.html
            $subject = wordwrap(substr($subject, 0, 998), 78, self::$nl);
            $this->subject = trim($subject);
        }
        return $this;
    }


    // dynamic configurable parameter
    public function format($type = 'text') {
        if (in_array($type, array('text', 'html'))) {
            $this->format = $type;
        }
        return $this;
    }


    // dynamic configurable parameter
    public function debug($flag = 0) {
        $this->debug = (bool)$flag;
        return $this;
    }


    public function body($data = '') {
        $this->body = trim($data);
        return $this;
    }


    public function attach($filepath = '') {
        if ($filepath) {
            if (!is_array($filepath)) {
                $filepath = array($filepath);
            }

            $this->attachments = $filepath;
        }
        return $this;
    }


    protected function build_headers() {
        $headers[] = 'From: '.$this->prepare_recipient($this->from[0]);
        $headers[] = 'Reply-To: '.($this->reply_to ? $this->prepare_recipient($this->reply_to[0]) : $this->prepare_recipient($this->from[0]));
        $headers[] = 'Subject: '.$this->subject;
        $headers[] = 'Date: '.date('r');
        $headers[] = 'User-Agent: LSMailer';

        $headers[] = $this->prepare_header_section('To');
        $headers[] = $this->prepare_header_section('CC');
        $headers[] = $this->prepare_header_section('BCC');

        // clean empty strings
        $headers = array_filter($headers, function($e) { return (bool)($e); });

        return implode(self::$nl, $headers) . self::$nl;
    }


    private function prepare_header_section($section = '') {
        $result = '';

        if (in_array($section, array('To', 'CC', 'BCC'))) {
            $sec = strtolower($section);
            $result = '';

            if ($this->{$sec}) {
                $elem = array_map(array($this, 'prepare_recipient'), $this->{strtolower($sec)});
                $result = sprintf("%s: %s", $section, implode(', ', $elem));
            }
        }

        return $result;
    }


    protected function build_content() {
        $this->prepare_body_section($this->format);
    }


    protected function prepare_body_section() {
        $has_attach = (bool)count($this->attachments);
        $is_html = ($this->format == 'html') ? 1 : 0;

        $this->content = '';
        $this->boundary = md5(uniqid(time()));

        # common part html or with attachments
        if ($has_attach || $is_html) {
            $this->prepare_multipart_section($has_attach);
        }

        $this->prepare_text_section();

        # only for html part
        if ($is_html) {
            $this->content .= $this->prepare_boundary_line();
            $this->prepare_html_section();
        }

        if ($is_html || $has_attach) {
            $this->content .= $this->prepare_boundary_line(!$has_attach);
        }

        # only for attachments
        if ($has_attach) {
            $this->content = $this->prepare_attachments_section();
        }

        return $this->content;
    }


    private function prepare_attachments_section() {
        foreach($this->attachments as $k => $attach) {
            $final_item = (bool)($k == (count($this->attachments) - 1));

            $this->content .= $this->prepare_attachment($attach);
            $this->content .= $this->prepare_boundary_line($final_item);
        }

        return $this->content;
    }

    private function prepare_text_section() {
        $text = wordwrap(strip_tags($this->body), self::$wordwrap);

        $this->content .= sprintf('Content-Type: text/plain; charset=%s %s', $this->config->charset, self::$nl);

        $this->content .= self::$nl;
        $this->content .= sprintf('%s %s', $text, self::$nl);

        return $this->content;
    }


    private function prepare_html_section($content) {
        $this->content .= sprintf('Content-Type: text/html; charset=%s %s', $this->config->charset, self::$nl);

        $this->content .= self::$nl;
        $this->content .= sprintf('%s %s', $this->body, self::$nl);

        return $this->content;
    }


    private function prepare_multipart_section($has_attach = false) {
        $type = ($has_attach) ? 'mixed' : 'alternative';

        $this->content .= sprintf('MIME-Version: 1.0 %s', self::$nl);
        $this->content .= sprintf('Content-Type: multipart/%s; boundary="%s" %s', $type, $this->boundary, self::$nl);

        $this->content .= self::$nl;
        $this->content .= sprintf('This is a multi-part message in MIME format.%s', self::$nl);
        $this->content .= sprintf('Content-Type: multipart/alternative; boundary="%s" %s', $this->boundary, self::$nl);
        $this->content .= $this->prepare_boundary_line();

        return $this->content;
    }


    private function prepare_attachment($attachment) {
        $attach = '';

        if (file_exists($attachment) && is_readable($attachment)) {
            $file = file_get_contents($attachment);
            $attach = 'Content-Type: application/octet-stream'.self::$nl;
            $attach .= 'Content-Transfer-Encoding: Base64'.self::$nl;
            $attach .= sprintf('Content-Disposition: attachment; filename=%s%s', basename($attachment), self::$nl);
            $attach .= self::$nl;
            $attach .= chunk_split(base64_encode($file));
            $attach .= self::$nl;
        }

        return $attach;
    }


    private function prepare_boundary_line($final = false) {
        $pattern = ($final) ? '--%s--' : '--%s';
        $ln = sprintf($pattern, $this->boundary);

        return sprintf("%s%s%s", self::$nl, $ln, self::$nl);
    }


    private function prepare_recipient(array $recipient) {
        return ltrim(sprintf('%s<%s>', $recipient['name'], $recipient['email']));
    }

    private function parse_emails($section, $emails = '') {
        if ($emails) {
            if (is_string($emails)) {
                $emails = array($emails);
            }

            foreach($emails as $email_strings) {
                $this->{$section}[] = $this->parse_single_email($email_strings);
            }
        }
        return $this;
    }


    private function parse_single_email($email_str = '') {
        $parts = explode(self::$sep, $email_str);
        $emailfull = array(
            'email' => trim($parts[0]),
            'name' => isset($parts[1]) ? trim($parts[1]) : ''
        );

        return $emailfull;
    }


    private function connect() {
        $this->connection = fsockopen($this->config->host, $this->config->port, $errno, $errstr, self::$timeout);
        if ($errno) {
            throw new \Exception(sprintf("Could not connect, errno: %s with message: %s", $errno, $errstr));
        }

        $this->in();

        $resp = $this->out('EHLO '.self::$sending_host);
        if ($resp[1] != 250 || !$resp[0]) {
            return $this->printer(_::MSG_EHLONOTACCEPTED);
        }

        return ($this->connect_tls() && $this->connect_auth());
    }


    private function connect_auth() {
        if ($this->config->user && $this->config->pass) {
            $r = $this->out('AUTH LOGIN');

            if ($r[1] !== 334) {
                return $this->printer(_::MSG_AUTHLOGINFAILED);
            }

            $u = $this->out(base64_encode($this->config->user));
            if ($u[1] !== 334) {
                return $this->printer(_::MSG_AUTHLOGINFAILEDUSER);
            }

            $p = $this->out(base64_encode($this->config->pass));
            if ($p[1] !== 235) {
                return $this->printer(_::MSG_AUTHLOGINFAILEDPASS);
            }
        }
        return true;
    }


    private function connect_tls() {
        if ($this->config->secure === 'tls') {
            $r = $this->out('STARTTLS');

            if ($r[1] !== 220) {
                return false;
            }

            $sr = stream_socket_enable_crypto($this->connection, true, STREAM_CRYPTO_METHOD_TLS_CLIENT);
            if ($sr === false) {
                return $this->printer(_::MSG_UNABLESTARTTLS);
            }

            $q = $this->out('EHLO '.self::$sending_host);
            if ($q[1] !== 250) {
                return false;
            }
        }
        return true;
    }


    private function deliver() {
        // $this->implement check for compulsory elements

        $this->out(sprintf("MAIL FROM: <%s>", $this->from[0]['email']));

        $recipients = array_merge($this->to, $this->cc, $this->bcc);
        foreach ($recipients as $r) {
            $r = $this->out(sprintf("RCPT TO: <%s>", $r['email']));
            if($r[1] != 250) {
                return false;
            }
        }

        $d = $this->out('DATA');
        if ($d[1] != 354) {
            return $this->printer(_::MSG_DATANOTACCEPTED);
        }

        $o = $this->out($this->headers . $this->content . self::$nl . '.' .self::$nl); // final point added

        if ($o[1] != 250) {
            return $this->printer(_::MSG_DATANOTACCEPTED);
        }

        return true;
    }


    private function disconnect() {
        $this->out('QUIT');
        fclose($this->connection);
    }

    private function out($string = '') {
        if ($this->connection) {
            $this->printer($string);
            fputs($this->connection, $string . self::$nl);

            return $this->in();
        } else {
            return false;
        }
    }


    private function in() {
        if ($this->connection) {
            $response = '';
            while ($str = fgets($this->connection, 4096)) {
                $response .= $str;
                if (substr($str, 3, 1) === ' ') break;
            }

            $code = (int)substr($response, 0, 3);
            $this->printer($response);

            return array($response, $code);
        } else {
            return false;
        }
    }


    private function printer($msg = '') {
        if ($this->debug) {
            fputs(STDOUT, $msg.PHP_EOL);
        }
        return false;
    }
}
