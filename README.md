# LSMAILER

## What is LSMailer?

LSMailer is a modern and efficient library to send emails using SMTP driver. It can be use in simple projects, where there is a need for a simple way to send emails (as in contact forms, polls, etc).

It has very intuitive methods, as to(), cc(), bcc(), send(). More than that, these methods are chainable, so you can always play in an elegant way with the parameters.

It works with simple, TLS or SSL SMTP accounts. Debugging option also available (just use the debug flag).


## Requirements


At least PHP 5.4.

Optionally, [phpunit](http://phpunit.de) to run the test suite.


## Installation


If you're using composer, then add this to your _composer.json_ and run `composer update`.

    "lemonsoftware/lsmailer"

Otherwise, in your code, include `autoload.php` file from this package:

    require '<path>/<to>/<package>/vendor/autoload.php';


## Before you start

You may need to update an internal value for sending host (identified by its fully qualified DNS host name - for example _mail.example.com_). Open `LSMailer/Drivers/LSSmtp.php` in a text editor of your choice.

            $sending_host = 'localhost';

replace it by:

            $sending_host = 'mail.example.com';


## Configuration


### Example of static config

Configuration is pretty straightforward, you need to supply the server information and credentials in a `stdClass` structure. For example, before sending an email in your code you must have:

    	<?php

		....
		$conf = new \stdClass;
		$conf->host = 'mail.example.com';
		$conf->port = 465;
		$conf->secure = 'ssl';
		$conf->user = 'user';
		$conf->pass = 'secret';
		$conf->charset = 'ISO-8859-2';
		$conf->timezone = 'UTC+2';


### Parameters explained:

* host - hostname where the mailserver is located (e.g. localhost, mail.example.com). If using ssl, don't prepend with ssl:// but use the secure flag (see below).
* port - _(default: 25)_ port on which the smtp service is running
* secure - _(default: none)_ `ssl|tls` -check your mailserver to see what's the preferred way
* user - username (only if smtp requires authorization - most of the cases)
* pass - password (only if smtp requires authorization - most of the cases)
* charset - _(default: UTF-8)_ character encoding for messages
* timezone - _(default: Europe/Amsterdam)_ - your timezone

From these parameters, only _host_, _port_ are required, added to _user_ and _pass_ if authorization used.


### Dynamic configuration

Available to be dynamically changes is the `format()` of the messages.

It can be `html|text` _(default:text)_

    $mailer->format('html');

Also, for testing purposes, you can use `debug()`

    $mailer->debug(1);

Debug messages will be outputted to console. Defaults to 0.

## Usage

After preparing the static configuration, you can use it like this:

        <?php
        require 'vendor/autoload.php';

        use LSMailer\LSMail;

        $conf = new \stdClass;
        $conf->host = 'smtp.gmail.com';
        $conf->port = 465;
        $conf->secure = 'ssl';
        $conf->user = 'mytestuser';
        $conf->pass = 's93nfg2mss12';
        $conf->timezone = 'Europe/Dublin';

        $mailer = LSMail::factory('smtp', $conf);
        $mailer->format('html'); // dynamic configuration

        $mailer
            ->to(array('john@example.com', 'john.doe@example.com; John Doe'))
            ->to('another.joe@example.com; Another Joe')
            ->from('jane.doe@mailserver.com');

        $mailer
            ->cc('jane@mailserver.com; Jane')
            ->reply_to('jane.doe@lsmailer.com');

        $mailer->subject("LSMailer - nice piece of software");
        $mailer->format('html')->debug(1);

        $mailer->body("<strong>Text and roses.</strong>");
        $mailer
            ->attach('/path/file1')
            ->attach(array('/path/file2', '/path/file3'));
        $mailer->send();

             ...

All methods are chainable, and with names very intuitive (they correspond to the email fields, as _To:_, _Cc:_, _ReplyTo:_).

